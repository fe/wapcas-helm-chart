{{/*
Expand the name of the chart.
*/}}
{{- define "wapcas.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "wapcas.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "wapcas.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "wapcas.labels" -}}
helm.sh/chart: {{ include "wapcas.chart" . }}
{{ include "wapcas.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "wapcas.selectorLabels" -}}
app.kubernetes.io/name: {{ include "wapcas.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "wapcas.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "wapcas.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the MongoDB connection string from values.
*/}}
{{- define "wapcas.mongoDbConnectionString" -}}
{{- if .Values.mongodb.enabled }}
{{- printf "mongodb://%s:%s@%s:%d/" .Values.mongodb.auth.rootUser .Values.mongodb.auth.rootPassword .Values.mongodb.service.nameOverride (.Values.mongodb.service.ports.mongodb | int) | quote }}
{{- else }}
{{- print (.Values.mongodbUrl | required ".Values.mongodbUrl is required if the built-in MongoDB deployment is disabled.") | quote }}
{{- end }}
{{- end }}
