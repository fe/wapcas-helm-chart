k3d cluster delete wapcas
k3d cluster create wapcas \
  --registry-create registry:0.0.0.0:5000 \
  --port "8888:80@loadbalancer" \
  --agents 1
